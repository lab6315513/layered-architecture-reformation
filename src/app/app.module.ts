import { Module } from '@nestjs/common';

import { GuardModule } from 'src/guards';

import { ControllerModule } from 'src/controllers';

import { JwtHelperModule } from 'src/shared';

import { DatabaseModule } from './database';

@Module({
  imports: [
    DatabaseModule,
    JwtHelperModule,
    GuardModule,
    ControllerModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
