import { TypeOrmModuleOptions, TypeOrmOptionsFactory } from "@nestjs/typeorm";

export class DatabaseConfig implements TypeOrmOptionsFactory {
    createTypeOrmOptions(connectionName?: string): TypeOrmModuleOptions {
        return {
            type:'sqlite',
            database: 'test.sqlite',
            entities: [__dirname+'/../../entities/*.entity{.ts,.js}'],
            synchronize: true,
        };
    }
}