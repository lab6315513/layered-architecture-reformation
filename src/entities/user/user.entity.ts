import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryColumn, UpdateDateColumn } from "typeorm";

import { TUserEntity, YN } from "src/shared";

@Entity({ name: 'users' })
export class UserEntity implements TUserEntity {
    @PrimaryColumn()
    id: string;

    @Column({ comment: '클라우드 사업자 코드' })
    cloudProviderCode: string;

    @Column({ comment: '닉네임' })
    nickname: string;

    @Column({ comment: '이미지 파일 ID' })
    imageId: string;

    @Column({ comment: '활성화 여부', default: 'Y' })
    activateYn: YN;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;

    @DeleteDateColumn()
    deletedAt: Date;

    constructor() {
        console.log('UserEntityImpl constructor');
    }
}