import { Body, Controller, Get, Inject, Logger, Param, Post } from "@nestjs/common";

import { IJwtHelper, JWT_HELPER, Public, TAddUserReq } from "src/shared";

import { TermsService, UserService, UserTokenService } from "src/services";

@Controller({ path: 'users', version: '1' })
export class UserController {

    private readonly logger = new Logger(UserController.name);

    constructor(
        @Inject(JWT_HELPER)
        private readonly jwtHelper: IJwtHelper,
        private readonly termsService: TermsService,
        private readonly userService: UserService,
        private readonly userTokenService: UserTokenService
    ) { }

    @Public()
    @Get(':id')
    async getUserDetail(@Param('id') id: string) {
        this.logger.debug(id);
        return this.userService.getUserDetail(id);
    }

    @Post()
    async addUser(@Body() dto: TAddUserReq) {
        const user = await this.userService.addUser(dto);
        await this.termsService.agreementTerms(user, dto.termsList);
        const tokens = await this.jwtHelper.signin(user.id);
        await this.userTokenService.addUserToken(user, tokens.refreshToken);
        return tokens;
    }
}