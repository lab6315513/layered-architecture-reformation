import { CanActivate, ExecutionContext, Inject, Injectable, Logger } from "@nestjs/common";
import { Reflector } from "@nestjs/core";

import { Request } from "express";
import { IncomingHttpHeaders } from "http";

import { IJwtHelper, JWT_HELPER } from "src/shared";


@Injectable()
export class AuthGuard implements CanActivate {

    private readonly logger = new Logger(AuthGuard.name);

    constructor(
        private readonly reflector: Reflector,
        @Inject(JWT_HELPER)
        private readonly jwtHelper: IJwtHelper
    ) {}

    canActivate(context: ExecutionContext): boolean {
        const isPublic: string = this.reflector.get('public', context.getHandler());
        if (isPublic) {
            return true;
        }

        const request: Request = context.switchToHttp().getRequest();
        const headers: IncomingHttpHeaders = request.headers;
        const authorization: string = headers.authorization;
        this.logger.debug(authorization);
        return true;
    }
}