export type TAddUserDto = {
    username: string;
    password: string;
    passCode: string;
    name: string;
    phone: string;
    birthday: string;
    cloudProviderCode?: string;
}