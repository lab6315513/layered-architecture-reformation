import { Inject, Injectable, NotFoundException } from "@nestjs/common";

import { TUserEntity } from "src/shared";

import { IUserRepository, TCreateUserDto, USER_REPOSITORY } from "src/repositories";

import { TAddUserDto } from "./user.dto";

@Injectable()
export class UserService {

    constructor(
        @Inject(USER_REPOSITORY)
        private readonly userRepository: IUserRepository
    ) {}

    async getUserDetail(id: string) {
        const user: TUserEntity = await this.userRepository.findByUserId(id);
        if (!user) {
            throw new NotFoundException('회원을 찾을 수 없습니다.');
        }
        return user;
    }

    async addUser(dto: TAddUserDto) {
        const createUserDto: TCreateUserDto = {
            ...dto,
            nickname: 'nickname'
        };
        return await this.userRepository.createUser(createUserDto);
    }
}