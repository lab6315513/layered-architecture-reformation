import { Module } from "@nestjs/common";

import { RepositoryModule } from "src/repositories";

import { TermsService } from "./terms";
import { UserService } from "./user";
import { UserTokenService } from "./user-token";

@Module({
    imports: [
        RepositoryModule
    ],
    providers: [
        UserService,
        UserTokenService,
        TermsService
    ],
    exports: [
        UserService,
        UserTokenService,
        TermsService
    ]
})
export class ServiceModule {}