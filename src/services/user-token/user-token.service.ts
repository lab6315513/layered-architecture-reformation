import { Injectable, Logger } from "@nestjs/common";

import { TUserEntity } from "src/shared";

@Injectable()
export class UserTokenService {

    private readonly logger = new Logger(UserTokenService.name);

    constructor() {}

    addUserToken(user: TUserEntity, refreshToken: any) {
        this.logger.debug('[addUserToken] 리프레시 토큰 등록');
    }
}