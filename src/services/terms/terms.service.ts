import { Injectable, Logger } from "@nestjs/common";

import { TUserEntity } from "src/shared";

@Injectable()
export class TermsService {

    private readonly logger = new Logger(TermsService.name);

    async agreementTerms(user: TUserEntity, termsList: string[]) {
        this.logger.debug('[agreementTerms] 회원 약관 동의');
    }
}