export type YN = 'Y' | 'N';

export type RecordDate = {
    createdAt: Date;
    updatedAt: Date;
}

export type DeletionDate = {
    deletedAt: Date;
}