export type TAddUserReq = {
    username: string;
    password: string;
    passCode: string;
    name: string;
    phone: string;
    birthday: string;
    termsList: string[];
    cloudProviderCode?: string;
}

export type TCreateUserDto = TAddUserReq & {
    nickname: string;
}