import { DeletionDate, RecordDate, YN } from "../common";

export type TUserEntity = RecordDate & DeletionDate & {
    id: string; // 회원 ID
    cloudProviderCode: string; // 클라우드 사업자 코드
    nickname: string; // 닉네임
    imageId: string; // 이미지 파일 ID
    activateYn: YN; // 활성화 여부
}