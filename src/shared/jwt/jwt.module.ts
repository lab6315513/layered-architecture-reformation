import { Global, Module } from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";

import { JwtHelper } from "./jwt.helper";
import { JWT_HELPER } from "./jwt.helper.interface";

@Global()
@Module({
    providers: [
        JwtService,
        { provide: JWT_HELPER, useClass: JwtHelper }
    ],
    exports: [
        JWT_HELPER
    ]
})
export class JwtHelperModule {}