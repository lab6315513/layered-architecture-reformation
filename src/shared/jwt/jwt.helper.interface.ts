export interface IJwtHelper {
    signin(aud: string): any;
    verify(): any;
}

export const JWT_HELPER = 'JWT_HELPER';