import { Injectable, Logger } from "@nestjs/common";
import { IJwtHelper } from "./jwt.helper.interface";

@Injectable()
export class JwtHelper implements IJwtHelper {

    private readonly logger = new Logger(JwtHelper.name);

    signin(): any {
        this.logger.debug('[signin] 토큰 발행');
        return null;
    }
    
    verify(): any {
        return null;
    }
}