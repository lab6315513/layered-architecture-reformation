import { CustomDecorator, SetMetadata } from "@nestjs/common";

export type TRoles = 'guest' | 'user';

export const Roles = (roles: TRoles): CustomDecorator<string> => {
    return SetMetadata('roles', roles);
}