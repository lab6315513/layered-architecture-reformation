import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";

import { UserEntity } from "src/entities";

import { USER_REPOSITORY } from "./user";
import { UserRepository } from "./user/user.repository";

@Module({
    imports: [
        TypeOrmModule.forFeature([
            UserEntity
        ])
    ],
    providers: [
        { provide: USER_REPOSITORY, useClass: UserRepository }
    ],
    exports: [
        USER_REPOSITORY
    ]
})
export class RepositoryModule {}