import { TUserEntity } from "src/shared";

export type TCreateUserDto = {
    username: string;
    password: string;
    passCode: string;
    name: string;
    phone: string;
    birthday: string;
    cloudProviderCode?: string;
    nickname: string;
}

export interface IUserRepository {
    findByUserId(id: string): Promise<TUserEntity>;

    createUser(request: TCreateUserDto): Promise<TUserEntity>;
}

export const USER_REPOSITORY = 'USER_REPOSITORY';

