import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

import { UserEntity } from "src/entities";

import { TUserEntity } from 'src/shared';

import { TCreateUserDto, IUserRepository } from "./user.repository.interface";

export class UserRepository implements IUserRepository {

    constructor(
        @InjectRepository(UserEntity)
        private readonly repository: Repository<TUserEntity>
    ) { }

    findByUserId(id: string): Promise<TUserEntity> {
        return this.repository.findOne({
            where: {
                id
            }
        });
    }

    createUser(dto: TCreateUserDto): Promise<TUserEntity> {
        const { name, nickname, birthday, passCode, username, password, phone, cloudProviderCode } = dto;
        const newUser = this.repository.create({
            cloudProviderCode: cloudProviderCode,
            nickname,
            // profile: {
            //     passCode,
            //     name,
            //     phone,
            //     birthday
            // },
            // auth: {
            //     username,
            //     password,
            //     passwordUpdatedAt: new Date()
            // },
        });
        return this.repository.save(newUser);
    }
}